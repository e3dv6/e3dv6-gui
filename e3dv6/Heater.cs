﻿using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6
{
    public class Heater
    {
        private SerialPort serial;

        public Heater(SerialPort serial)
        {
            this.serial = serial;
        }
        
        public double GetCurrentTemperature()
        {
            Log.Debug("Fetching current temperature.");
            return Requester.SendRequest(serial, Commands.GET_CURRENT_TEMPERATURE).AsDouble();
        }

        public int GetTargetTemperature()
        {
            Log.Debug("Fetching target temperature.");
            return Requester.SendRequest(serial, Commands.GET_TARGET_TEMPERATURE).AsInt();
        }

        public void SetTargetTemperature(int temperature)
        {
            Log.Debug("Setting target temperature.");
            Requester.SendRequest(serial, Commands.SET_TARGET_TEMPERATURE, temperature);
        }

        public bool IsHeaterOn()
        {
            Log.Debug("Fetching heater status.");
            return Requester.SendRequest(serial, Commands.IS_HEATER_ON).AsBoolean();
        }

        public double GetPercentage()
        {
            Log.Debug("Fetching heater percentage.");
            return Requester.SendRequest(serial, Commands.GET_HEATER_PERCENTAGE).AsDouble();
        }
        
    }
}
