﻿using e3dv6.types;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6.utils
{
    public class TemperatureLog
    {
        private ConcurrentBag<RecordedTemperature> recordedTemperatures = new ConcurrentBag<RecordedTemperature>();

        public void Add(RecordedTemperature recordedTemperature)
        {
            recordedTemperatures.Add(recordedTemperature);
        }

        public ConcurrentBag<RecordedTemperature> GetAll()
        {
            return recordedTemperatures;
        }

    }
}
