﻿using e3dv6.types;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6.utils
{
    public class Log
    {
        private static ConcurrentBag<LogMessage> messages = new ConcurrentBag<LogMessage>();

        public static void Debug(String message)
        {
            messages.Add(new LogMessage(DateTime.Now, message, MessageType.Debug));
        }

        public static void Info(String message)
        {
            messages.Add(new LogMessage(DateTime.Now, message, MessageType.Info));
        }

        public static void Error(String message)
        {
            messages.Add(new LogMessage(DateTime.Now, message, MessageType.Error));
        }

        public static void Error(String message, Exception e)
        {
            messages.Add(new LogMessage(DateTime.Now, message, MessageType.Error, e));
        }

        public static ConcurrentBag<LogMessage> GetMessages()
        {
            return messages;
        }

    }
}
