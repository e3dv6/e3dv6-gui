﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace e3dv6
{
    public class WindowUtil
    {
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        private static readonly IntPtr HWND_TOP = new IntPtr(0);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        public static void AlwaysOntop(IntPtr windowHandle, bool ontop)
        {
            if (ontop)
            {
                SetWindowPos(windowHandle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
            } else
            {
                SetWindowPos(windowHandle, HWND_NOTOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
            }
        }

        public static void MoveToLowerRight(Form form)
        {
            var rightmost = Screen.AllScreens.OrderBy(s => s.WorkingArea.Right).Last();
            form.Left = rightmost.WorkingArea.Right - form.Width;
            form.Top = rightmost.WorkingArea.Bottom - form.Height;
        }
    }
}
