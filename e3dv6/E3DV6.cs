﻿using e3dv6.types;
using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace e3dv6
{
    public class E3DV6
    {
        public delegate void ConnectionStatus(object sender, EventArgs e);
        public event ConnectionStatus OnConnectionStatusChanged;

        private SerialPort serial = new SerialPort();
        private ThreadStart autoReconnectThreadStart;
        private Thread authReconnectThread;
        private ThreadStart connectionStatusThreadStart;
        private Thread connectionStatusThread;
        private Pinger pinger;
        private Heater heater;
        private HeaterManager heaterManager;
        private List<Fan> fans = new List<Fan>();

        public E3DV6()
        {
            pinger = new Pinger(serial);
            heater = new Heater(serial);
            heaterManager = new HeaterManager(serial);
            fans.Add(new Fan(serial, FanType.Fan1));
            fans.Add(new Fan(serial, FanType.Fan2));
            fans.Add(new Fan(serial, FanType.Fan3));

            serial.BaudRate = 9600;
            serial.ReadBufferSize = 8096;
            serial.ReadTimeout = 10000;
            serial.WriteTimeout = 10000;

            autoReconnectThreadStart = new ThreadStart(AutoReconnectThread);
            authReconnectThread = new Thread(autoReconnectThreadStart);
            authReconnectThread.Start();

            connectionStatusThreadStart = new ThreadStart(UpdateConnectionStatus);
            connectionStatusThread = new Thread(connectionStatusThreadStart);
            connectionStatusThread.Start();
        }

        public void Connect()
        {
            if (!serial.IsOpen)
            {
                Log.Info("Trying to connect to the arduino.");
                FindPortAndConnect();
            }
        }

        public void FindPortAndConnect()
        {
            for (int i = 1; i < 10; i++) {
                String portName = "COM" + i;
                Log.Debug("Trying to connect on port " + portName);

                try
                {
                    serial.PortName = portName;
                    serial.Open();
                    pinger.SendPing();
                    Log.Info("Found arduino and connected to it.");
                    return;
                }catch(Exception e) {
                    Log.Debug("Failed to connect to arduino on port " + portName + ". " + e.Message);
                }
            }

            throw new Exception("Failed to find port name for E3DV6 arduino.");
        }
        
        public void Disconnect()
        {
            if (serial.IsOpen)
            {
                Log.Info("Disconnecting from arduino.");
                serial.Close();
            }
        }

        public Pinger Pinger {
            get
            {
                return pinger;
            }
        }

        public Heater Heater
        {
            get
            {
                return heater;
            }
        }

        public HeaterManager HeaterManager
        {
            get
            {
                return heaterManager;
            }
        }

        public List<Fan> Fans
        {
            get
            {
                return fans;
            }
        }

        public Fan GetFan(FanType fanType)
        {
            foreach(Fan fan in fans) {
                if (fan.FanType == fanType)
                {
                    return fan;
                }
            }

            return null;
        }

        public bool IsConnected { get; private set; }

        public void UpdateConnectionStatus()
        {
            while (true)
            {
                if (IsConnected != serial.IsOpen)
                {
                    IsConnected = serial.IsOpen;
                    RaiseConnectionStatusEvent(IsConnected);
                }

                Thread.Sleep(1000);
            }
        }

        private void RaiseConnectionStatusEvent(bool isConnected)
        {
            if (OnConnectionStatusChanged != null)
            {
                OnConnectionStatusChanged(this, new ConnectionStatusEventArgs(isConnected));
            }
        }

        public bool AutoReconnect { get; set; }

        private void AutoReconnectThread()
        {
            while (true)
            {
                if (AutoReconnect && !serial.IsOpen)
                {
                    try
                    {
                        Log.Debug("Reconnecting to Arduino.");
                        Connect();
                    } catch(Exception)
                    {

                    }

                    Thread.Sleep(2000);
                }
            }
        }

    }
}
