﻿using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6
{
    public static class Requester
    {
        private static Char startCommand = 'a';
        private static Char errorResponse = 'q';

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static Response SendRequest(SerialPort serial, Commands command)
        {
            return SendRequest(serial, command, "000");
        }

        public static Response SendRequest(SerialPort serial, Commands command, int value)
        {
            return SendRequest(serial, command, value.ToString("X3").ToLower());
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static Response SendRequest(SerialPort serial, Commands command, String value)
        {
            StringBuilder message = new StringBuilder();
            message.Append(startCommand);
            message.Append((char)command);
            message.Append(value);
            message.Append('\n');

            Log.Debug("Sending message '" + message.ToString() + "' to Arduino.");

            serial.Write(message.ToString());
            String response = serial.ReadLine();

            Log.Debug("Received message '" + response.ToString() + "' from Arduino.");

            validateResponse(response);
            return parseResponse(response);
        }

        private static void validateResponse(String response)
        {
            if (response[0] == errorResponse)
            {
                throw new Exception("Received error from e3dv6 arduino.");
            }
        }

        private static Response parseResponse(String response)
        {
            String data = response.Substring(2);
            return new Response(Commands.GET_CURRENT_TEMPERATURE, data);
        }

    }
}
