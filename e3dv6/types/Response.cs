﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6
{
    public class Response
    {
        private CultureInfo customCulture;
        private String data;

        public Response(Commands command) : this(command, null)
        {
            
        }

        public Response(Commands command, String data)
        {
            this.Command = command;
            this.data = data;
            customCulture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
        }

        public int AsInt()
        {
            return Convert.ToInt32(data, 16);
        }

        public double AsDouble()
        {
            try
            {
                return Convert.ToDouble(data, customCulture);
            } catch (Exception e)
            {
                throw new FormatException("Failed to convert value to double, value was not in a correct format. Failed to convert the following value: " + data + ".");
            }
        }

        public String AsString()
        {
            return data;
        }
        public bool AsBoolean()
        {
            if (data == "1")
            {
                return true;
            } else if (data == "0")
            {
                return false;
            } else
            {
                throw new Exception("Invalid value, unable to convert response value to bool.");
            }
        }

        public Commands Command { get; private set; }

    }
}
