﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6.types
{
    public class ConnectionStatusEventArgs : EventArgs
    {
        public ConnectionStatusEventArgs(bool isConnected)
        {
            IsConnected = isConnected;
        }

        public bool IsConnected { get; private set; }

    }
}
