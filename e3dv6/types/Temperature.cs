﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6.types
{
    public class RecordedTemperature
    {

        public RecordedTemperature(DateTime date, double temperature)
        {
            this.Date = date;
            this.Temperature = temperature;
        }

        public DateTime Date { get; private set; }
        public double Temperature { get; private set; }

    }
}
