﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6.types
{
    public class LogMessage
    {
        public LogMessage(DateTime date, String message, MessageType type)
        {
            this.Date = date;
            this.Message = message;
            this.Type = type;
        }

        public LogMessage(DateTime date, String message, MessageType type, Exception e)
        {
            this.Date = date;
            this.Message = message + e.Message;
            this.Type = type;
        }

        public DateTime Date { get; private set; }
        public MessageType Type { get; private set; }
        public String Message { get; private set; }

        override
        public String ToString()
        {
            return Date.ToString() + ": " + Type.ToString().PadRight(5) + ": " + Message;
        }
    }
}
