﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6.types
{
    public enum MessageType
    {
        Debug,
        Info,
        Error
    }
}
