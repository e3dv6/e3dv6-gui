﻿using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6
{
    public class Pinger
    {
        private SerialPort serial;

        public Pinger(SerialPort serial)
        {
            this.serial = serial;
        }

        public void SendPing()
        {
            Log.Debug("Sending ping message.");
            Requester.SendRequest(serial, Commands.PONG);
        }

    }
}
