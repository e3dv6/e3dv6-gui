﻿using e3dv6.forms;
using e3dv6.types;
using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace e3dv6
{
    public partial class Overview : Form
    {
        private E3DV6 e3dv6 = new E3DV6();
        private Color okColor = Color.FromArgb(222, 250, 231);
        private Color warningColor = Color.FromArgb(245, 247, 173);
        private Color errorColor = Color.FromArgb(247, 173, 173);
        private ThreadStart updateThreadStart;
        private Thread updateThread;
        private TemperatureLog currentTemperatureLog = new TemperatureLog();
        private TemperatureLog targetTemperatureLog = new TemperatureLog();
        private bool isFormClosing = false;

        public Overview()
        {
            InitializeComponent();
        }

        private void Overview_Load(object sender, EventArgs e)
        {
            WindowUtil.MoveToLowerRight(this);
            WindowUtil.AlwaysOntop(this.Handle, true);

            e3dv6.OnConnectionStatusChanged += E3dv6_OnConnectionStatusChanged;
            e3dv6.AutoReconnect = true;

            updateThreadStart = new ThreadStart(Update);
            updateThread = new Thread(updateThreadStart);

            lblHeating.BringToFront();
        }

        private void E3dv6_OnConnectionStatusChanged(object sender, EventArgs e)
        {
            ConnectionStatusEventArgs connectionStatus = (ConnectionStatusEventArgs)e;
            if (connectionStatus.IsConnected)
            {
                this.BackColor = Color.FromArgb(222, 250, 231);
                Log.Info("Connection to E3DV6 (arduino) established.");

                if (!updateThread.IsAlive)
                {
                    updateThread.Start();
                }
            } else
            {
                this.BackColor = Color.FromArgb(247, 173, 173);
                Log.Error("Lost connection to E3DV6 (arduino)");
            }
        }

        private void Update()
        {
            while (!isFormClosing)
            {
                try
                {
                    //UpdateTargetTemperature();
                    UpdateCurrentTemperature();
                    //UpdateIsHeating();
                    //UpdateHeaterPercentage();
                    //UpdateFan1Speed();
                    //UpdateFan2Speed();
                    //UpdateFan3Speed();
                    Thread.Sleep(500);
                }catch(Exception exception)
                {
                    Log.Error("An error occured when updating the heater and target, current temperature.", exception);
                }
            }
        }

        private void UpdateFan1Speed()
        {
            try
            {
                int fanSpeed1 = e3dv6.GetFan(FanType.Fan1).GetSpeed();
                lblFan1.Invoke(new Action(() =>
                {
                    lblFan1.Text = fanSpeed1.ToString() + "%";
                    lblFan1.BackColor = okColor;
                }));
            } catch(Exception e)
            {
                lblFan1.Invoke(new Action(() =>
                {
                    lblFan1.BackColor = errorColor;
                }));
            }
        }

        private void UpdateFan2Speed()
        {
            try
            {
                int fanSpeed2 = e3dv6.GetFan(FanType.Fan2).GetSpeed();
                lblFan2.Invoke(new Action(() =>
                {
                    lblFan2.Text = fanSpeed2.ToString() + "%";
                    lblFan2.BackColor = okColor;
                }));
            }
            catch (Exception e)
            {
                lblFan2.Invoke(new Action(() =>
                {
                    lblFan2.BackColor = errorColor;
                }));
            }
        }

        private void UpdateFan3Speed()
        {
            try
            {
                int fanSpeed3 = e3dv6.GetFan(FanType.Fan3).GetSpeed();
                lblFan3.Invoke(new Action(() =>
                {
                    lblFan3.Text = fanSpeed3.ToString() + "%";
                    lblFan3.BackColor = okColor;
                }));
            }
            catch (Exception e)
            {
                lblFan3.Invoke(new Action(() =>
                {
                    lblFan3.BackColor = errorColor;
                }));
            }
        }

        private void UpdateCurrentTemperature()
        {
            try
            {
                double currentTemperature = e3dv6.Heater.GetCurrentTemperature();
                Log.Info("The current temperature is " + currentTemperature);
                currentTemperatureLog.Add(new RecordedTemperature(DateTime.Now, currentTemperature));

                lblCurrentTemperature.Invoke(new Action(() =>
                {
                    lblCurrentTemperature.Text = Math.Round(currentTemperature, 2).ToString();
                    if (IsTemperatureBetweenTarget())
                    {
                        lblCurrentTemperature.BackColor = okColor;
                        lblHeaterPercentage.BackColor = okColor;
                        lblHeating.BackColor = okColor;
                    } else
                    {
                        lblCurrentTemperature.BackColor = warningColor;
                        lblHeaterPercentage.BackColor = warningColor;
                        lblHeating.BackColor = warningColor;
                    }
                }));

                lblCurrentTemperatureUpdated.Invoke(new Action(() =>
                {
                    lblCurrentTemperatureUpdated.Text = "Updated: " + DateTime.Now;
                }));
            }
            catch(Exception exception)
            {
                Log.Error("An error occured when updating the current temperature.", exception);

                lblCurrentTemperature.Invoke(new Action(() =>
                {
                    lblCurrentTemperature.BackColor = errorColor;
                    lblHeaterPercentage.BackColor = errorColor;
                    lblHeating.BackColor = errorColor;
                }));
            }
        }

        private void UpdateTargetTemperature()
        {
            try
            {
                int targetTemperature = e3dv6.Heater.GetTargetTemperature();
                Log.Info("The target temperature is " + targetTemperature);

                lblTargetTemperature.Invoke(new Action(() =>
                {
                    lblTargetTemperature.Text = targetTemperature.ToString();
                    if (lblTargetTemperature.Text == "0")
                    {
                        lblTargetTemperature.BackColor = warningColor;
                    } else
                    {
                        lblTargetTemperature.BackColor = okColor;
                    }
                }));

                lblTargetTemperatureUpdated.Invoke(new Action(() =>
                {
                    lblTargetTemperatureUpdated.Text = "Updated: " + DateTime.Now;
                }));
            }
            catch (Exception exception)
            {
                Log.Error("An error occured when updating the target temperature.", exception);

                lblTargetTemperature.Invoke(new Action(() =>
                {
                    lblTargetTemperature.BackColor = errorColor;
                }));
            }
        }

        public void UpdateIsHeating()
        {
            try
            {
                bool isHeatingUp = e3dv6.Heater.IsHeaterOn();
                Log.Info("The heater is " + (isHeatingUp ? "on" : "off"));

                lblHeating.Invoke(new Action(() =>
                {
                    lblHeating.Visible = isHeatingUp;
                    lblHeating.ForeColor = Color.Black;
                }));
            }
            catch (Exception exception)
            {
                Log.Error("An error occured when updating the status of the heater.", exception);

                lblHeating.Invoke(new Action(() => {
                    lblHeating.ForeColor = Color.Red;
                    lblHeating.Visible = true;
                }));
            }
        }

        private void UpdateHeaterPercentage()
        {
            try
            {
                double percentage = e3dv6.Heater.GetPercentage() * 100;
                Log.Info("The heater percentage is " + percentage.ToString());

                lblHeaterPercentage.Invoke(new Action(() =>
                {
                    lblHeaterPercentage.Text = Math.Round(percentage, 0).ToString() + "%";
                    lblHeaterPercentage.ForeColor = Color.Black;
                    lblHeaterPercentage.Visible = (percentage != 0);
                    lblHeaterPercentage.BringToFront();
                }));
            }
            catch (Exception exception)
            {
                Log.Error("An error occured when updating the heater percentage.", exception);

                lblHeaterPercentage.Invoke(new Action(() => {
                    lblHeaterPercentage.ForeColor = Color.Red;
                    lblHeaterPercentage.Visible = true;
                    lblHeaterPercentage.Text = "Error";
                    lblHeaterPercentage.BringToFront();
                }));
            }
        }

        private void lblTargetTemperature_Click(object sender, EventArgs e)
        {
            new SetTargetTemperatureForm(e3dv6).Show();
        }

        private bool IsTemperatureBetweenTarget()
        {
            double currentTemperature = Double.Parse(lblCurrentTemperature.Text);
            int targetTemperature = Int32.Parse(lblTargetTemperature.Text);
            int span = 1;

            return currentTemperature >= targetTemperature - span && currentTemperature <= targetTemperature + span;
        }
        
        private void menuViewLog_Click(object sender, EventArgs e)
        {
            new LogForm().Show();
        }

        private void menuViewChart_Click(object sender, EventArgs e)
        {
            new TemperatureChartForm(currentTemperatureLog, targetTemperatureLog).Show();
        }

        private void menuAutoConnect_Click(object sender, EventArgs e)
        {
            menuAutoConnect.Checked = !menuAutoConnect.Checked;
            e3dv6.AutoReconnect = menuAutoConnect.Checked;
        }

        private void menuAlwaysOntop_Click(object sender, EventArgs e)
        {
            menuAlwaysOntop.Checked = !menuAlwaysOntop.Checked;
            WindowUtil.AlwaysOntop(this.Handle, menuAlwaysOntop.Checked);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void lblFan1_Click(object sender, EventArgs e)
        {
            new FanSpeedForm(e3dv6, FanType.Fan1).Show();
        }

        private void lblFan2_Click(object sender, EventArgs e)
        {
            new FanSpeedForm(e3dv6, FanType.Fan2).Show();
        }

        private void lblFan3_Click(object sender, EventArgs e)
        {
            new FanSpeedForm(e3dv6, FanType.Fan3).Show();
        }

        private void Overview_FormClosing(object sender, FormClosingEventArgs e)
        {
            isFormClosing = true;
        }

        private void menuViewHeaterManager_Click(object sender, EventArgs e)
        {
            new HeaterManagerForm(e3dv6).Show();
        }
    }
}
