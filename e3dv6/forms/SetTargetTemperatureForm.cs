﻿using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace e3dv6
{
    public partial class SetTargetTemperatureForm : Form
    {
        private E3DV6 e3dv6;

        public SetTargetTemperatureForm(E3DV6 e3dv6)
        {
            this.e3dv6 = e3dv6;
            InitializeComponent();
        }

        private void btnSetTargetTemperature_Click(object sender, EventArgs e)
        {
            SetTargetTemperature();
        }

        private void SetTargetTemperature()
        {
            int temperature;

            if (!GetTargetTemperature(out temperature))
            {
                MessageBox.Show("Invalid temperature input.");
            }

            try
            {
                e3dv6.Heater.SetTargetTemperature(temperature);
                Log.Info("Setting the target temperature to " + temperature + ".");
            } catch (Exception e)
            {
                MessageBox.Show("Failed to set target temperature.");
            }

            this.Close();
        }

        private bool GetTargetTemperature(out int temperature)
        {
            return Int32.TryParse(txtTargetTemperature.Text, out temperature);
        }

        private void txtTargetTemperature_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetTargetTemperature();
            }
        }
    }
}
