﻿using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace e3dv6.forms
{
    public partial class HeaterManagerForm : Form
    {
        private E3DV6 e3dv6;

        public HeaterManagerForm(E3DV6 e3dv6)
        {
            InitializeComponent();
            this.e3dv6 = e3dv6;
        }

        private void btnSetProperties_Click(object sender, EventArgs e)
        {
            SetProperties();
        }

        private void SetProperties()
        {
            int decay, thermalResistance, heaterIncrease;

            if (!GetDecay(out decay))
            {
                MessageBox.Show("Invalid decay input.");
            }

            if (!GetHeaterIncrease(out heaterIncrease))
            {
                MessageBox.Show("Invalid heater increase input.");
            }

            if (!GetThermalResistance(out thermalResistance))
            {
                MessageBox.Show("Invalid thermal resistance input.");
            }

            try
            {
                e3dv6.HeaterManager.SetDecay(decay);
                e3dv6.HeaterManager.SetHeaterIncrease(heaterIncrease);
                e3dv6.HeaterManager.SetThermalResistance(thermalResistance);
                Log.Info("Setting heater manager properties.");
            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to set heater mangager properties.");
                Log.Error("Failed to set heater manager properties. ", e);
            }

            this.Close();
        }

        private bool GetDecay(out int decay)
        {
            return Int32.TryParse(txtDecay.Text, out decay);
        }

        private bool GetHeaterIncrease(out int heaterIncrease)
        {
            return Int32.TryParse(txtHeaterIncrease.Text, out heaterIncrease);
        }

        private bool GetThermalResistance(out int thermalResistance)
        {
            return Int32.TryParse(txtThermalResistance.Text, out thermalResistance);
        }

        private void btnSetProperties_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetProperties();
            }
        }

    }
}
