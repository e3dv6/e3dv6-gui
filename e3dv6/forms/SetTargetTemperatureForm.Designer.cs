﻿namespace e3dv6
{
    partial class SetTargetTemperatureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSetTargetTemperature = new System.Windows.Forms.Button();
            this.txtTargetTemperature = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSetTargetTemperature
            // 
            this.btnSetTargetTemperature.Location = new System.Drawing.Point(115, 141);
            this.btnSetTargetTemperature.Name = "btnSetTargetTemperature";
            this.btnSetTargetTemperature.Size = new System.Drawing.Size(353, 73);
            this.btnSetTargetTemperature.TabIndex = 2;
            this.btnSetTargetTemperature.Text = "Set Target Temperature";
            this.btnSetTargetTemperature.UseVisualStyleBackColor = true;
            this.btnSetTargetTemperature.Click += new System.EventHandler(this.btnSetTargetTemperature_Click);
            // 
            // txtTargetTemperature
            // 
            this.txtTargetTemperature.Location = new System.Drawing.Point(30, 78);
            this.txtTargetTemperature.Name = "txtTargetTemperature";
            this.txtTargetTemperature.Size = new System.Drawing.Size(524, 38);
            this.txtTargetTemperature.TabIndex = 1;
            this.txtTargetTemperature.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTargetTemperature_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "Target Temperature";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(559, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "°C";
            // 
            // SetTargetTemperatureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 244);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTargetTemperature);
            this.Controls.Add(this.btnSetTargetTemperature);
            this.Name = "SetTargetTemperatureForm";
            this.Text = "Target Temperature";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSetTargetTemperature;
        private System.Windows.Forms.TextBox txtTargetTemperature;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}