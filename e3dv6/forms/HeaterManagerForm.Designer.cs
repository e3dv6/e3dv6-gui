﻿namespace e3dv6.forms
{
    partial class HeaterManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSetProperties = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDecay = new System.Windows.Forms.TextBox();
            this.txtHeaterIncrease = new System.Windows.Forms.TextBox();
            this.txtThermalResistance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSetProperties
            // 
            this.btnSetProperties.Location = new System.Drawing.Point(225, 346);
            this.btnSetProperties.Name = "btnSetProperties";
            this.btnSetProperties.Size = new System.Drawing.Size(275, 85);
            this.btnSetProperties.TabIndex = 0;
            this.btnSetProperties.Text = "Set Properties";
            this.btnSetProperties.UseVisualStyleBackColor = true;
            this.btnSetProperties.Click += new System.EventHandler(this.btnSetProperties_Click);
            this.btnSetProperties.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSetProperties_KeyDown);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtHeaterIncrease, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtThermalResistance, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtDecay, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(719, 327);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // txtDecay
            // 
            this.txtDecay.Location = new System.Drawing.Point(3, 39);
            this.txtDecay.Name = "txtDecay";
            this.txtDecay.Size = new System.Drawing.Size(703, 38);
            this.txtDecay.TabIndex = 0;
            // 
            // txtHeaterIncrease
            // 
            this.txtHeaterIncrease.Location = new System.Drawing.Point(3, 149);
            this.txtHeaterIncrease.Name = "txtHeaterIncrease";
            this.txtHeaterIncrease.Size = new System.Drawing.Size(703, 38);
            this.txtHeaterIncrease.TabIndex = 1;
            // 
            // txtThermalResistance
            // 
            this.txtThermalResistance.Location = new System.Drawing.Point(3, 259);
            this.txtThermalResistance.Name = "txtThermalResistance";
            this.txtThermalResistance.Size = new System.Drawing.Size(703, 38);
            this.txtThermalResistance.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 218);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 32);
            this.label1.TabIndex = 3;
            this.label1.Text = "Thermal Resistance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = "Heater Increase";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 32);
            this.label3.TabIndex = 5;
            this.label3.Text = "Decay";
            // 
            // HeaterManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 443);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnSetProperties);
            this.Name = "HeaterManagerForm";
            this.Text = "HeaterManagerForm";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSetProperties;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHeaterIncrease;
        private System.Windows.Forms.TextBox txtThermalResistance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDecay;
    }
}