﻿using e3dv6.types;
using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace e3dv6.forms
{
    public partial class LogForm : Form
    {
        private static List<MessageType> filters = new List<MessageType> { MessageType.Info, MessageType.Debug, MessageType.Error };

        public LogForm()
        {
            InitializeComponent();
        }

        private void Log_Load(object sender, EventArgs e)
        {
            cmbMessageType.DataSource = filters;
            UpdateMessages();
        }

        private void UpdateMessages()
        {
            lstMessages.DataSource = Log.GetMessages()
                .Where(message => message.Type == (MessageType)cmbMessageType.SelectedItem)
                .OrderByDescending(message => message.Date)
                .ToList();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateMessages();
        }

        private void cmbMessageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateMessages();
        }
    }
}
