﻿using e3dv6.types;
using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace e3dv6.forms
{
    public partial class TemperatureChartForm : Form
    {
        private TemperatureLog targetTemperatureLog;
        private TemperatureLog currentTemperatureLog;
        private ThreadStart updateThreadStart;
        private Thread updateThread;
        private bool updateTemperatures = true;

        public TemperatureChartForm(TemperatureLog currentTemperatureLog, TemperatureLog targetTemperatureLog)
        {
            InitializeComponent();
            this.currentTemperatureLog = currentTemperatureLog;
            this.targetTemperatureLog = targetTemperatureLog;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TemperatureGraphForm_Load(object sender, EventArgs e)
        {
            Axis xaxis = temperatureChart.ChartAreas[0].AxisX;
            xaxis.IntervalType = DateTimeIntervalType.Minutes;
            xaxis.LabelStyle.Format = "HH:mm";
            xaxis.Interval = 2;
            UpdateChartSize();

            updateThreadStart = new ThreadStart(Update);
            updateThread = new Thread(updateThreadStart);
            updateThread.Start();
        }

        private void Update()
        {
            while (updateTemperatures)
            {
                //List<RecordedTemperature> recordedTemperatures = currentTemperatureLog.GetAll().ToList();
                //temperatureChart.Series["ABC"].Points.DataBind(currentTemperatureLog.GetAll(), "Test", "Test 2", "Test 3");
                temperatureChart.Invoke(new Action(() =>
                {
                    temperatureChart.DataSource = null;
                    temperatureChart.DataSource = currentTemperatureLog.GetAll();
                    temperatureChart.Refresh();
                }));

                Thread.Sleep(1000);
            }
        }
        
        private void TemperatureChartForm_Resize(object sender, EventArgs e)
        {
            UpdateChartSize();
        }
        
        private void UpdateChartSize()
        {
            temperatureChart.Height = this.Height - 50;
            temperatureChart.Width = this.Width;
        }

        private void TemperatureChartForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            updateTemperatures = false;
        }
    }
}
