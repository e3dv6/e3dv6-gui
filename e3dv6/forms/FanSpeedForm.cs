﻿using e3dv6.types;
using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace e3dv6.forms
{
    public partial class FanSpeedForm : Form
    {
        private E3DV6 e3dv6;
        private FanType fanType;

        public FanSpeedForm(E3DV6 e3dv6, FanType fanType)
        {
            this.e3dv6 = e3dv6;
            this.fanType = fanType;
            InitializeComponent();
        }

        private void btnSetFanSpeed_Click(object sender, EventArgs e)
        {
            SetFanSpeed();
        }

        private void SetFanSpeed()
        {
            int fanSpeed;

            if (!GetFanSpeed(out fanSpeed))
            {
                MessageBox.Show("Invalid fan speed input.");
            }

            try
            {
                e3dv6.GetFan(fanType).SetSpeed(fanSpeed);
                Log.Info("Setting the fan speed to " + fanSpeed+ ".");
            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to set fan speed.");
                Log.Error("Failed to set fan speed. ", e);
            }

            this.Close();
        }

        private bool GetFanSpeed(out int fanSpeed)
        {
            return Int32.TryParse(txtFanSpeed.Text, out fanSpeed);
        }

        private void txtFanSpeed_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetFanSpeed();
            }
        }
    }
}
