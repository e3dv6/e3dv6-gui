﻿namespace e3dv6
{
    partial class Overview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serial = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTargetTemperatureUpdated = new System.Windows.Forms.Label();
            this.lblCurrentTemperatureUpdated = new System.Windows.Forms.Label();
            this.lblTargetTemperature = new System.Windows.Forms.Label();
            this.lblCurrentTemperature = new System.Windows.Forms.Label();
            this.lblHeaterPercentage = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.menuAutoConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAlwaysOntop = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewLog = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewChart = new System.Windows.Forms.ToolStripMenuItem();
            this.menuViewHeaterManager = new System.Windows.Forms.ToolStripMenuItem();
            this.lblHeating = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblFan1 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblFan2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblFan3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(277, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(389, 46);
            this.label1.TabIndex = 2;
            this.label1.Text = "Current Temperature";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(737, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(371, 46);
            this.label2.TabIndex = 4;
            this.label2.Text = "Target Temperature";
            // 
            // lblTargetTemperatureUpdated
            // 
            this.lblTargetTemperatureUpdated.AutoSize = true;
            this.lblTargetTemperatureUpdated.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblTargetTemperatureUpdated.Location = new System.Drawing.Point(717, 226);
            this.lblTargetTemperatureUpdated.Name = "lblTargetTemperatureUpdated";
            this.lblTargetTemperatureUpdated.Size = new System.Drawing.Size(403, 32);
            this.lblTargetTemperatureUpdated.TabIndex = 7;
            this.lblTargetTemperatureUpdated.Text = "Updated: 0000-00-00 00:00:00";
            // 
            // lblCurrentTemperatureUpdated
            // 
            this.lblCurrentTemperatureUpdated.AutoSize = true;
            this.lblCurrentTemperatureUpdated.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblCurrentTemperatureUpdated.Location = new System.Drawing.Point(279, 226);
            this.lblCurrentTemperatureUpdated.Name = "lblCurrentTemperatureUpdated";
            this.lblCurrentTemperatureUpdated.Size = new System.Drawing.Size(403, 32);
            this.lblCurrentTemperatureUpdated.TabIndex = 8;
            this.lblCurrentTemperatureUpdated.Text = "Updated: 0000-00-00 00:00:00";
            // 
            // lblTargetTemperature
            // 
            this.lblTargetTemperature.BackColor = System.Drawing.Color.Transparent;
            this.lblTargetTemperature.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTargetTemperature.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblTargetTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTargetTemperature.ForeColor = System.Drawing.Color.Black;
            this.lblTargetTemperature.Location = new System.Drawing.Point(733, 121);
            this.lblTargetTemperature.Margin = new System.Windows.Forms.Padding(10);
            this.lblTargetTemperature.MinimumSize = new System.Drawing.Size(400, 98);
            this.lblTargetTemperature.Name = "lblTargetTemperature";
            this.lblTargetTemperature.Padding = new System.Windows.Forms.Padding(10);
            this.lblTargetTemperature.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTargetTemperature.Size = new System.Drawing.Size(400, 98);
            this.lblTargetTemperature.TabIndex = 9;
            this.lblTargetTemperature.Text = "0";
            this.lblTargetTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTargetTemperature.Click += new System.EventHandler(this.lblTargetTemperature_Click);
            // 
            // lblCurrentTemperature
            // 
            this.lblCurrentTemperature.AutoSize = true;
            this.lblCurrentTemperature.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrentTemperature.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCurrentTemperature.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblCurrentTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentTemperature.ForeColor = System.Drawing.Color.Black;
            this.lblCurrentTemperature.Location = new System.Drawing.Point(285, 121);
            this.lblCurrentTemperature.Margin = new System.Windows.Forms.Padding(3, 0, 30, 0);
            this.lblCurrentTemperature.MinimumSize = new System.Drawing.Size(400, 98);
            this.lblCurrentTemperature.Name = "lblCurrentTemperature";
            this.lblCurrentTemperature.Padding = new System.Windows.Forms.Padding(10, 0, 30, 0);
            this.lblCurrentTemperature.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCurrentTemperature.Size = new System.Drawing.Size(400, 98);
            this.lblCurrentTemperature.TabIndex = 10;
            this.lblCurrentTemperature.Text = "0";
            // 
            // lblHeaterPercentage
            // 
            this.lblHeaterPercentage.AutoSize = true;
            this.lblHeaterPercentage.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeaterPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaterPercentage.ForeColor = System.Drawing.Color.DimGray;
            this.lblHeaterPercentage.Location = new System.Drawing.Point(331, 151);
            this.lblHeaterPercentage.Name = "lblHeaterPercentage";
            this.lblHeaterPercentage.Size = new System.Drawing.Size(76, 36);
            this.lblHeaterPercentage.TabIndex = 11;
            this.lblHeaterPercentage.Text = "33%";
            this.lblHeaterPercentage.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1167, 52);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.menuAutoConnect,
            this.menuAlwaysOntop});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(136, 48);
            this.toolStripMenuItem1.Text = "Options";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(322, 6);
            // 
            // menuAutoConnect
            // 
            this.menuAutoConnect.Checked = true;
            this.menuAutoConnect.CheckState = System.Windows.Forms.CheckState.Checked;
            this.menuAutoConnect.Name = "menuAutoConnect";
            this.menuAutoConnect.Size = new System.Drawing.Size(325, 46);
            this.menuAutoConnect.Text = "Auto connect";
            this.menuAutoConnect.Click += new System.EventHandler(this.menuAutoConnect_Click);
            // 
            // menuAlwaysOntop
            // 
            this.menuAlwaysOntop.Checked = true;
            this.menuAlwaysOntop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.menuAlwaysOntop.Name = "menuAlwaysOntop";
            this.menuAlwaysOntop.Size = new System.Drawing.Size(325, 46);
            this.menuAlwaysOntop.Text = "Always on-top";
            this.menuAlwaysOntop.Click += new System.EventHandler(this.menuAlwaysOntop_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuViewLog,
            this.menuViewChart,
            this.menuViewHeaterManager});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(94, 48);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // menuViewLog
            // 
            this.menuViewLog.Name = "menuViewLog";
            this.menuViewLog.Size = new System.Drawing.Size(347, 46);
            this.menuViewLog.Text = "Log";
            this.menuViewLog.Click += new System.EventHandler(this.menuViewLog_Click);
            // 
            // menuViewChart
            // 
            this.menuViewChart.Name = "menuViewChart";
            this.menuViewChart.Size = new System.Drawing.Size(347, 46);
            this.menuViewChart.Text = "Chart";
            this.menuViewChart.Click += new System.EventHandler(this.menuViewChart_Click);
            // 
            // menuViewHeaterManager
            // 
            this.menuViewHeaterManager.Name = "menuViewHeaterManager";
            this.menuViewHeaterManager.Size = new System.Drawing.Size(347, 46);
            this.menuViewHeaterManager.Text = "Heater Manager";
            this.menuViewHeaterManager.Click += new System.EventHandler(this.menuViewHeaterManager_Click);
            // 
            // lblHeating
            // 
            this.lblHeating.AutoSize = true;
            this.lblHeating.BackColor = System.Drawing.SystemColors.Window;
            this.lblHeating.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeating.ForeColor = System.Drawing.Color.Black;
            this.lblHeating.Location = new System.Drawing.Point(290, 124);
            this.lblHeating.Name = "lblHeating";
            this.lblHeating.Size = new System.Drawing.Size(60, 69);
            this.lblHeating.TabIndex = 15;
            this.lblHeating.Text = "↑";
            this.lblHeating.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::e3dv6.Properties.Resources.cpu_fan_128;
            this.pictureBox1.Location = new System.Drawing.Point(21, 71);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 56);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lblFan1
            // 
            this.lblFan1.BackColor = System.Drawing.Color.Transparent;
            this.lblFan1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFan1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblFan1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFan1.ForeColor = System.Drawing.Color.Black;
            this.lblFan1.Location = new System.Drawing.Point(20, 70);
            this.lblFan1.Margin = new System.Windows.Forms.Padding(10);
            this.lblFan1.MinimumSize = new System.Drawing.Size(180, 0);
            this.lblFan1.Name = "lblFan1";
            this.lblFan1.Padding = new System.Windows.Forms.Padding(10);
            this.lblFan1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFan1.Size = new System.Drawing.Size(186, 56);
            this.lblFan1.TabIndex = 17;
            this.lblFan1.Text = "0%";
            this.lblFan1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblFan1.Click += new System.EventHandler(this.lblFan1_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 52);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 228);
            this.splitter1.TabIndex = 18;
            this.splitter1.TabStop = false;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label4.Location = new System.Drawing.Point(246, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(2, 200);
            this.label4.TabIndex = 19;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::e3dv6.Properties.Resources.cpu_fan_128;
            this.pictureBox2.Location = new System.Drawing.Point(21, 137);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(56, 56);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            // 
            // lblFan2
            // 
            this.lblFan2.BackColor = System.Drawing.Color.Transparent;
            this.lblFan2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFan2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblFan2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFan2.ForeColor = System.Drawing.Color.Black;
            this.lblFan2.Location = new System.Drawing.Point(20, 136);
            this.lblFan2.Margin = new System.Windows.Forms.Padding(10);
            this.lblFan2.MinimumSize = new System.Drawing.Size(180, 56);
            this.lblFan2.Name = "lblFan2";
            this.lblFan2.Padding = new System.Windows.Forms.Padding(10);
            this.lblFan2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFan2.Size = new System.Drawing.Size(186, 56);
            this.lblFan2.TabIndex = 21;
            this.lblFan2.Text = "0%";
            this.lblFan2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblFan2.Click += new System.EventHandler(this.lblFan2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::e3dv6.Properties.Resources.cpu_fan_128;
            this.pictureBox3.Location = new System.Drawing.Point(21, 203);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(56, 56);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 22;
            this.pictureBox3.TabStop = false;
            // 
            // lblFan3
            // 
            this.lblFan3.BackColor = System.Drawing.Color.Transparent;
            this.lblFan3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFan3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblFan3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFan3.ForeColor = System.Drawing.Color.Black;
            this.lblFan3.Location = new System.Drawing.Point(20, 202);
            this.lblFan3.Margin = new System.Windows.Forms.Padding(10);
            this.lblFan3.MinimumSize = new System.Drawing.Size(180, 56);
            this.lblFan3.Name = "lblFan3";
            this.lblFan3.Padding = new System.Windows.Forms.Padding(10);
            this.lblFan3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFan3.Size = new System.Drawing.Size(186, 56);
            this.lblFan3.TabIndex = 23;
            this.lblFan3.Text = "0%";
            this.lblFan3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblFan3.Click += new System.EventHandler(this.lblFan3_Click);
            // 
            // Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(173)))), ((int)(((byte)(173)))));
            this.ClientSize = new System.Drawing.Size(1167, 280);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lblFan3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblFan2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblFan1);
            this.Controls.Add(this.lblHeating);
            this.Controls.Add(this.lblHeaterPercentage);
            this.Controls.Add(this.lblCurrentTemperature);
            this.Controls.Add(this.lblTargetTemperature);
            this.Controls.Add(this.lblCurrentTemperatureUpdated);
            this.Controls.Add(this.lblTargetTemperatureUpdated);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Overview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "E3D-V6 Heater";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Overview_FormClosing);
            this.Load += new System.EventHandler(this.Overview_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTargetTemperatureUpdated;
        private System.Windows.Forms.Label lblCurrentTemperatureUpdated;
        private System.Windows.Forms.Label lblTargetTemperature;
        private System.Windows.Forms.Label lblCurrentTemperature;
        private System.Windows.Forms.Label lblHeaterPercentage;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem menuAutoConnect;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuViewLog;
        private System.Windows.Forms.ToolStripMenuItem menuViewChart;
        private System.Windows.Forms.ToolStripSeparator optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuAlwaysOntop;
        private System.Windows.Forms.Label lblHeating;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblFan1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblFan2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblFan3;
        private System.Windows.Forms.ToolStripMenuItem menuViewHeaterManager;
    }
}

