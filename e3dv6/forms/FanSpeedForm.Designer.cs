﻿namespace e3dv6.forms
{
    partial class FanSpeedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtFanSpeed = new System.Windows.Forms.TextBox();
            this.btnSetFanSpeed = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 32);
            this.label1.TabIndex = 5;
            this.label1.Text = "Fan Speed";
            // 
            // txtFanSpeed
            // 
            this.txtFanSpeed.Location = new System.Drawing.Point(18, 67);
            this.txtFanSpeed.Name = "txtFanSpeed";
            this.txtFanSpeed.Size = new System.Drawing.Size(353, 38);
            this.txtFanSpeed.TabIndex = 4;
            this.txtFanSpeed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFanSpeed_KeyDown);
            // 
            // btnSetFanSpeed
            // 
            this.btnSetFanSpeed.Location = new System.Drawing.Point(18, 125);
            this.btnSetFanSpeed.Name = "btnSetFanSpeed";
            this.btnSetFanSpeed.Size = new System.Drawing.Size(353, 73);
            this.btnSetFanSpeed.TabIndex = 6;
            this.btnSetFanSpeed.Text = "Set Fan Speed";
            this.btnSetFanSpeed.UseVisualStyleBackColor = true;
            this.btnSetFanSpeed.Click += new System.EventHandler(this.btnSetFanSpeed_Click);
            // 
            // FanSpeedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 226);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFanSpeed);
            this.Controls.Add(this.btnSetFanSpeed);
            this.Name = "FanSpeedForm";
            this.Text = "FanSpeedForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFanSpeed;
        private System.Windows.Forms.Button btnSetFanSpeed;
    }
}