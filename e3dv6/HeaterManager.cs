﻿using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace e3dv6
{
    public class HeaterManager
    {
        private SerialPort serial;

        public HeaterManager(SerialPort serial)
        {
            this.serial = serial;
        }

        public void SetDecay(int decay)
        {
            Log.Debug("Setting decay.");
            Requester.SendRequest(serial, Commands.SET_HEATER_DECAY, decay);
        }

        public void SetThermalResistance(int thermalResistance)
        {
            Log.Debug("Setting thermal resistance.");
            Requester.SendRequest(serial, Commands.SET_HEATER_THERMAL_RESISTANCE, thermalResistance);
        }

        public void SetHeaterIncrease(int heaterIncrease)
        {
            Log.Debug("Setting heater increase.");
            Requester.SendRequest(serial, Commands.SET_HEATER_INCREASE, heaterIncrease);
        }

    }
}
