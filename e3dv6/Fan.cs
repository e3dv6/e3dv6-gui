﻿using e3dv6.types;
using e3dv6.utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e3dv6
{
    public class Fan
    {
        private SerialPort serial;
        private FanType fanType;

        public Fan(SerialPort serial, FanType fanType)
        {
            this.serial = serial;
            this.fanType = fanType;
        }

        public void SetSpeed(int percentage)
        {
            Log.Info("Setting fan speed for fan '" + fanType + "'.");

            switch (fanType)
            {
                case FanType.Fan1:
                    Requester.SendRequest(serial, Commands.SET_FAN1_SPEED, percentage);
                    break;
                case FanType.Fan2:
                    Requester.SendRequest(serial, Commands.SET_FAN2_SPEED, percentage);
                    break;
                case FanType.Fan3:
                    Requester.SendRequest(serial, Commands.SET_FAN3_SPEED, percentage);
                    break;
                default:
                    throw new Exception("Unknown fan type '" + fanType + '"');
            }
        }

        public int GetSpeed()
        {
            Log.Info("Getting fan speed for fan '" + fanType + "'.");

            switch (fanType)
            {
                case FanType.Fan1:
                    return Requester.SendRequest(serial, Commands.GET_FAN1_SPEED).AsInt();
                case FanType.Fan2:
                    return Requester.SendRequest(serial, Commands.GET_FAN2_SPEED).AsInt();
                case FanType.Fan3:
                    return Requester.SendRequest(serial, Commands.GET_FAN3_SPEED).AsInt();
                default:
                    throw new Exception("Unknown fan type '" + fanType + '"');
            }
        }

        public FanType FanType
        {
            get
            {
                return fanType;
            }
        }

    }
}
